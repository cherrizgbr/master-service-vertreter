package de.cherriz.master.service.vertreter;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.ws.rs.NotAcceptableException;

/**
 * Controller fuer den REST Service fuer Mails.
 *
 * @author Frederik Kirsch
 */
@RestController
@RequestMapping(value = "/vertreter")
public class VertreterController {

    private final VertreterService vertreterService;

    /**
     * Setzt den Service für die Verarbeitung.
     *
     * @param vertreterService Der Vertreterservice.
     */
    @Inject
    public VertreterController(final VertreterService vertreterService) {
        this.vertreterService = vertreterService;
    }

    /**
     * Methode dient zur Pruefung der Verfuegbarkeit des Services.
     *
     * @return den Status des Services.
     */
    @RequestMapping(value = "/systemcheck", method = RequestMethod.GET)
    public String hello() {
        String result = "SYSTEMCHECK:";
        result += "EntityManager: " + (this.vertreterService != null ? "OK" : "Error");
        return result;
    }

    /**
     * Liefert zu einer Adresse den zugeordneten Vertreter.
     *
     * @param strasse Die Strasse.
     * @param ort     Der Ort.
     * @param plz     Die PLZ.
     * @return Den Vertreter.
     * @throws NotAcceptableException Ist die PLZ 0, oder lauten Strasse oder Ort "Fehler".
     */
    @RequestMapping(value = "/gebiet/{strasse},{plz},{ort}", method = RequestMethod.GET)
    public Vertreter getByGebiet(@PathVariable String strasse, @PathVariable String ort, @PathVariable Integer plz) throws NotAcceptableException {
        if ("Fehler".equals(strasse) || "Fehler".equals(ort) || plz == 0) {
            throw new NotAcceptableException("Es ist keine Adresse definiert.");
        }
        return this.vertreterService.findByGebiet(strasse, ort, plz);
    }

    /**
     * Behandelt {@link javax.ws.rs.NotAcceptableException} und uebersetzt sie in den entsprechenden HTTP Status.
     *
     * @param exception Die Exception.
     * @return Der HTTP Status.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public String handleNotAcceptable(NotAcceptableException exception) {
        return exception.getMessage();
    }

}