package de.cherriz.master.service.vertreter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entitaet eines Vertreters.
 *
 * @author Frederik Kirsch
 */
@Entity
@XmlRootElement
public class Vertreter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long gebiet;

    private String vorname;

    private String nachname;

    /**
     * @return Die ID des Vertreters.
     */
    public Long getId() {
        return id;
    }

    /**
     * Setzt die ID des Vertreters.
     *
     * @param id Die VertreterID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Das Gebiet des Vertreters.
     */
    public Long getGebiet() {
        return gebiet;
    }

    /**
     * Setzt da Gebiet des Vertreters.
     *
     * @param gebiet Das Gebiet.
     */
    public void setGebiet(Long gebiet) {
        this.gebiet = gebiet;
    }

    /**
     * @return Der Vorname.
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Setzt den Vornamen des Vertreters.
     *
     * @param vorname Der Vorname.
     */
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    /**
     * @return Der Nachname.
     */
    public String getNachname() {
        return nachname;
    }

    /**
     * Setzt den Nachnamen des Vertreters.
     *
     * @param nachname Der Nachname.
     */
    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

}