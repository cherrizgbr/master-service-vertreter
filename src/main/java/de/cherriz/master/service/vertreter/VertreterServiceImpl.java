package de.cherriz.master.service.vertreter;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.inject.Inject;
import java.util.List;

/**
 * Die Implementierung des {@link de.cherriz.master.service.vertreter.VertreterService}.
 *
 * @author Frederik Kirsch
 */
@Service
@Validated
public class VertreterServiceImpl implements VertreterService {

    private final VertreterRepository repository;

    /**
     * Setzt das Repository fuer den Datenbankzugriff.
     *
     * @param repository Das Repository.
     */
    @Inject
    public VertreterServiceImpl(final VertreterRepository repository) {
        this.repository = repository;
    }

    /**
     * Looks up the closest salesman depending on the adress.
     * Dummy implementation, address is not uses. Returns first found item.
     *
     * @param strasse
     * @param ort
     * @param plz
     * @return
     */
    @Override
    public Vertreter findByGebiet(String strasse, String ort, Integer plz) {
        List<Vertreter> v = this.repository.findAll();
        if (v != null && v.size() > 0) {
            return v.get(0);
        }
        return null;
    }

}