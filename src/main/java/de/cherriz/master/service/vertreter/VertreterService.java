package de.cherriz.master.service.vertreter;

/**
 * Schnittstelle fuer das Verarbeiten von Vertretern.
 *
 * @author Frederik Kirsch
 */
public interface VertreterService {

    /**
     * Liefert zu einer Adresse den zugeordneten Vertreter.
     *
     * @param strasse Die Strasse.
     * @param ort Der Ort.
     * @param plz Die PLZ.
     * @return Der zugeordnete Vertreter.
     */
    Vertreter findByGebiet(String strasse, String ort, Integer plz);

}