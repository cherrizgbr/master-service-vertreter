package de.cherriz.master.service.vertreter;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Schnittstelle zum Repository fuer Vertreter.
 *
 * @author Frederik Kirsch
 */
public interface VertreterRepository extends JpaRepository<Vertreter, Long> {}